Assignments for RPI CSCI-4430/6969, Spring 2013
===============================================

This repository contains the assignments for [Carlos Varela][varela]'s
[course in Programming Languages][course] and [Albert Armea][aarmea]'s
attempts to complete them.

[varela]: http://www.cs.rpi.edu/~cvarela/
[course]: http://www.cs.rpi.edu/academics/courses/spring13/proglang/
[aarmea]: http://www.albertarmea.com

Warning
-------

This repository contains (working or non-working) solutions to homework
assignments. Unless the assignments have been replaced, I strongly discourage
downloading and reading this code if you are currently taking or plan to take
this course.

