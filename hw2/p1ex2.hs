module Main where

import MapReduce

pairs :: [(Char, [Int])]
pairs = [ ('a', [3, 5, 2, 1, 4, 6, 5, 2])
        , ('b', [2, 5, 2, 3])
        ]

mrMap :: Char -> [Int] -> [(Int, Int)]
mrMap key [] = []
mrMap key [x] = [(x, 1)]
mrMap key (x : xs) = (x, 1) : mrMap key xs

mrComp :: Int -> Int -> Bool
mrComp a b = a <= b

mrReduce :: Int -> [Int] -> [(Int, Int)]
mrReduce key [] = []
mrReduce key [x] = [(key, x)]
mrReduce key (x : xs) = [(key, x+(snd.head $ mrReduce key xs))]

main = do
  putStrLn . show $ mapReduce mrMap mrComp mrReduce pairs
