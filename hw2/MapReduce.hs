module MapReduce where

import Data.List

gatherHelper
  :: Eq keyType
  => keyType -> [valType] -> [(keyType, valType)]
  -> [(keyType, [valType])]
gatherHelper k vs ((k', v') : ps)
  = if k == k'
    then gatherHelper k (v' : vs) ps
    else (k, vs) : gatherHelper k' [v'] ps
gatherHelper k vs []
  = [(k, vs)]

gatherAdjacent :: Eq keyType => [(keyType, valType)] -> [(keyType, [valType])]
gatherAdjacent ((k, v) : ps) =
  gatherHelper k [v] ps
gatherAdjacent [] = []

mapReduce
  :: (Eq a, Ord a)
  => (keyType -> valType -> [(a, b)]) -- Map function
  -> (a -> a -> Bool) -- Compare function
  -> (a -> [b] -> [(d, c)]) -- Reduce function
  -> [(keyType, valType)] -- the original data
  -> [(d, c)] -- Return list
mapReduce mrMap mrCompare mrReduce input = output
  where
    compareHelper (k1, v1) (k2, v2) =
      if mrCompare k1 k2
        then if mrCompare k2 k1
          then EQ
          else LT
        else GT
    t1 = concatMap (\(key, val) -> mrMap key val) input
    t2 = gatherAdjacent $ sortBy compareHelper t1
    output = concatMap (\(key, vals) -> mrReduce key vals) t2
