module Main where

import MapReduce

pairs :: [(Int, Char)]
pairs = [ (2, 'a')
        , (3, 'b')
        , (1, 'c')
        , (3, 'd')
        , (1, 'a')
        , (4, 'b')
        , (2, 'c')
        , (1, 'd')
        ]

mrMap :: Int -> Char -> [(Int, Char)]
mrMap a b = [(a, b)]

mrComp :: Int -> Int -> Bool
mrComp a b = a <= b

mrReduce :: Int -> [Char] -> [(Int, [Char])]
mrReduce a b = [(a, b)]

main = do
  putStrLn . show $ mapReduce mrMap mrComp mrReduce pairs
