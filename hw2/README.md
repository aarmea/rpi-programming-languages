CSCI-4430 Assignment 2
======================

Submission by [Albert Armea](mailto:armeaa@rpi.edu), based on
[this problem](http://www.cs.rpi.edu/academics/courses/spring13/proglang/PA2.htm).

I decided to write this assignment using [Haskell](http://www.haskell.org/)
instead of Oz. I recommend downloading and installing
[the Haskell platform](http://www.haskell.org/platform/) for your operating
system to test my assignment submission.

Part 1: MapReduce engine
------------------------

The basic MapReduce engine is implemented in `mapreduce.hs`, which contains
the `mapReduce` function. Examples 1 and 2 are implemented in `p1ex1.hs` and
`p1ex2.hs`, respectively. The functions passed into `mapReduce` are prefixed
with `mr` to avoid name conflicts with Haskell's built-in `map` and `reduce`
functions.

Run the examples using the following procedure:

1. Compile the example with `ghc --make p1ex?.hs`.
2. Run the example with `./p1ex?`, or `p1ex?.exe` on Windows.

You can change the input data in the examples by editing the `pairs` variable
in the example's source code and recompiling.

Part 2: Search and Statistics with MapReduce
--------------------------------------------

### Problem 1: Search

Problem 1 is implemented in `p2search.hs`. Compile my solution with `ghc
--make p2search.hs`, and run the example with `./p2search $TEXT_FILE
$SEARCH_TERM`, where `$TEXT_FILE` refers to a plain text file containing the
text to be searched and `$SEARCH_TERM` is the word the program will search
for. I have provided the plain text of Milton's *On His Blindness* as
specified in the assignment in `milton.txt`.

To perform the example given in the problem, do `./p2search milton.txt is`.

### Problem 2: Statistics

I did not attempt Problem 2 in this version of the submission.
