module Main where

import System.Environment
import System.IO
import Data.Char
import Data.List

import MapReduce

isPunc :: Char -> Bool
isPunc = not . isAlphaNum

wordsNoPunc :: String -> [String]
wordsNoPunc s = case dropWhile isPunc s of
  "" -> []
  s' -> w : wordsNoPunc s''
    where
      (w, s'') = break isPunc s'


mrMap :: String -> Int -> String -> [(String, Int)]
mrMap searchTerm lineNumber string = elements
  where
    lineWords = filter (isInfixOf searchTerm) $ wordsNoPunc string
    elements = zip lineWords (repeat lineNumber)

mrComp :: String -> String -> Bool
mrComp a b =
  if (length a) == (length b) then a <= b
  else (length a) <= (length b)

mrReduce :: String -> [Int] -> [(String, [Int])]
mrReduce key x = [(key, x)]

searchText :: [(Int, String)] -> String -> [(String, [Int])]
searchText file searchTerm = mapReduce (mrMap searchTerm) mrComp mrReduce file

parseFile :: String -> [(Int, String)]
parseFile = zip [1..] . lines

main = do
  args <- getArgs
  case args of
    [fileName, searchTerm] -> do
      file <- readFile fileName
      putStrLn . show $ searchText (parseFile file) searchTerm
    _ -> usageExit
  where
    usageExit = do
      name <- getProgName
      putStrLn $ "Usage: " ++ name ++ " FILE TERM"
