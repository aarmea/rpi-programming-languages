CSCI-4430 Assignment 1
======================

Submission by [Albert Armea](armeaa@rpi.edu), based on
[this problem](http://www.cs.rpi.edu/academics/courses/spring13/proglang/PA1.htm).

Part 1: RPI Flu Epidemic
------------------------

The Prolog code for this assignment is contained entirely in `flu.pro`. Open
your Prolog interpreter (I used [SWI-Prolog](http://www.swi-prolog.org/)),
consult `flu.pro`, and enter `infected(eric, X).` in the console to find all
possible infection paths from Ashley to Eric.

I did not attempt any extra credit for this part of the assignment.

Part 2: Students in a Line with Natural Language
------------------------------------------------

The Prolog code for this assignment is contained within `students.pro`,
`students_grammar.pro`, and `read_line.pro`. Open your Prolog interpreter and
consult `students_grammar.pro`. You will automatically be given a natural
language console - use commands like `switch X with Y.`, `where is X in
line?`, and `what is the current order of the line?`. Consult `students.pro`
instead to use the commands from a Prolog, rather than a natural language,
environment.

Warning: attempting to switch two non-adjacent elements will cause an error
message and the program will quit shortly after.
