% replace(): replace a symbol in a list with another symbol
%   syntax: replace(OldElement, NewElement, OldList, ReturnList)

% Base case: replacing anything in an empty list returns the empty list
replace(_, _, [], []).

% Recursion 1: replace the first element
replace(OldElement, NewElement, [OldElement|OldTail], [NewElement|NewTail]) :-
  replace(OldElement, NewElement, OldTail, NewTail).

% Recursion 2: replace element in the middle
replace(OldElement, NewElement, [OldHead|OldTail], [OldHead|NewTail]) :-
  OldHead \= OldElement,
  replace(OldElement, NewElement, OldTail, NewTail).


% find(): get the index of an element in a list
%   syntax: find(Element, List, Index)

% Base case 0: empty list, return -1
find(_, [], Index) :- Index is -1.

% Base case 1: element is at head of list
find(Element, [Element|_], Index) :- Index is 1.

% Base case 2: element is at the tail of list
find(Element, [ListHead|Element], Index) :-
  Index is length([ListHead|Element]).

% Recursion: iterate over the list
find(Element, [_|ListTail], Index) :-
  find(Element, ListTail, TempIndex),
  TempIndex > 0,
  Index is TempIndex+1.


% adjacent(): check if two elements in a list are adjacent
adjacent(Element1, Element2, List) :-
  find(Element1, List, Element1Index),
  find(Element2, List, Element2Index),
  Element1Index > 0, Element2Index > 0,
  once(
    Element1Index =:= Element2Index+1;
    Element1Index+1 =:= Element2Index
  ).


% swap(): swap two elements in a list
swap(OldElement, NewElement, OldList, NewList) :-
  replace(OldElement, tempElement, OldList, TempList1),
  replace(NewElement, OldElement, TempList1, TempList2),
  replace(tempElement, NewElement, TempList2, NewList).


% swap_adjacent(): swap two elements only if they are directly adjacent
swap_adjacent(OldElement, NewElement, OldList, NewList) :-
  adjacent(OldElement, NewElement, OldList),
  swap(OldElement, NewElement, OldList, NewList).

% swap_failure(): give an error message if swap_adjacent fails
swap_failure(OldElement, NewElement) :-
  writef("%w is too far away from %w! Chaos ensues. Will quit in five seconds.\n",
    [OldElement, NewElement]),
  sleep(5), % Pause so the error is visible before we quit
  halt.
