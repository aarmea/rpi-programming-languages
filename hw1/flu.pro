% Information provided by the problem
sick(ashley).

talk(ashley, john).
talk(ashley, nate).
talk(nate, sam).
talk(nate, matt).
talk(nate, adam).
talk(adam, nate).
talk(adam, wayne).
talk(wayne, charles).
talk(wayne, george).
talk(charles, sam).
talk(george, adam).
talk(george, sam).
talk(george, matt).
talk(eric, charles).

% === Code ===

% Basic approach: I treat this problem as a graph, where each person is a node
% and a conversation is a undirected edge between two people. Then, to solve
% the problem, simply search from the known sick person (the source) to the
% person that can possibly be sick.

% Test whether two people talked directly to each other (graph edge)
conversed(PersonA, PersonB) :-
  talk(PersonA, PersonB);
  talk(PersonB, PersonA).

% Graph search from known sick person to tested person

% Graph walk base case: the next person is directly adjacent on the graph
walk(PersonA, PersonB, Path, [PersonB | Path]) :-
  conversed(PersonA, PersonB).

% Graph walk recursion: pick an arbitrary person PersonC and check if there is
% a path from PersonA to PersonC to PersonB recursively
walk(PersonA, PersonB, VisitedPeople, Path) :-
  conversed(PersonA, PersonC),
  PersonC \== PersonB,
  not(member(PersonC, VisitedPeople)),
  walk(PersonC, PersonB, [PersonC | VisitedPeople], Path).

% Find a path from KnownInfected to TestPerson and return it as Path
infected(KnownInfected, TestPerson, Path) :-
  walk(KnownInfected, TestPerson, [KnownInfected], TestPath),
  reverse(TestPath, Path).

% Find a Path from all given sick people to TestPerson
infected(TestPerson, Path) :-
  sick(KnownInfected),
  infected(KnownInfected, TestPerson, Path).
