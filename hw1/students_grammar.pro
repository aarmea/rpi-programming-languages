:- ['read_line'].
:- ['students'].

% Grammar (all words used in queries) as Prolog facts
word(command, switch).
word(command, swap).
word(question, what).
word(question, where).
word(adjective, current).
word(article, the).
word(noun, person).
word(noun, order).
word(noun, line).
word(noun, list).
word(preposition, with).
word(preposition, of).
word(preposition, in).
word(verb, is).
period(.).
question(?).

% parse_sentence: performs the action defined by Sentence on the List
%   Syntax: parse_sentence(Sentence, OldList, NewList).

% Case 1: Switch statement of the form "switch person X with person Y."
parse_sentence([Switch, Person1, X, With, Person2, Y, Punct], OldList, NewList) :-
  word(command, Switch),
  word(noun, Person1),
  word(preposition, With),
  word(noun, Person2),
  period(Punct),
  swap_adjacent(X, Y, OldList, NewList),
  true.

% Case 2: Switch statement of the form "switch X with Y."
parse_sentence([Switch, X, With, Y, Punct], OldList, NewList) :-
  word(command, Switch),
  word(preposition, With),
  period(Punct),
  once(
    swap_adjacent(X, Y, OldList, NewList);
    swap_failure(X, Y)
  ),
  true.

% Case 3: List query of the form "what is the current order of the line?"
%   Does not modify the line
parse_sentence([What, Is, The1, Current, Order, Of, The2, Line, Punct], List, List) :-
  word(question, What),
  word(verb, Is),
  word(article, The1),
  word(adjective, Current),
  word(noun, Order),
  word(preposition, Of),
  word(article, The2),
  word(noun, Line),
  question(Punct),
  write(List), nl,
  true.

% Case 4: List query of the form "what is the order?"
%   Does not modify the line
parse_sentence([What, Is, The1, Order, Punct], List, List) :-
  word(question, What),
  word(verb, Is),
  word(article, The1),
  word(noun, Order),
  question(Punct),
  write(List), nl,
  true.

% Case 5: Position query of the form "where is X in line?"
%   Does not modify the line
parse_sentence([Where, Is, X, In, Line, Punct], List, List) :-
  word(question, Where),
  word(verb, Is),
  word(preposition, In),
  word(noun, Line),
  question(Punct),
  find(X, List, ListIndex),
  write(ListIndex), nl,
  true.

% Case 6: Position query of the form "where is X?"
parse_sentence([Where, Is, X, Punct], List, List) :-
  word(question, Where),
  word(verb, Is),
  question(Punct),
  find(X, List, ListIndex),
  writef("%w is at position %w\n", [X, ListIndex]),
  true.

% parse_failure(): Print an error message if the user input is invalid
parse_failure(List, List) :-
  writef("I didn\'t understand you.\n").

% Main application loop
main(List) :-
  read_line(Sentence),
  once(
    % Don't fail because the user did not provide a valid statement.
    parse_sentence(Sentence, List, NewList);
    parse_failure(List, NewList)
  ),
  main(NewList).

% Automatically call the main application loop using the starting list
:- main([a,b,c,d,e,f]).
