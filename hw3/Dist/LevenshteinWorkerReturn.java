import java.util.TreeMap;
import java.util.LinkedList;
import java.util.Iterator;

class LevenshteinWorkerReturn implements java.io.Serializable {
  static final int MAX_VALUES = 3;

  // Because Java 1.2 does not have priority queues
  private TreeMap results;

  public LevenshteinWorkerReturn() {
    results = new TreeMap();
  }

  public LevenshteinWorkerReturn(StringPair[] pairs, int[] rets) {
    // Put the input as a TreeMap from rets to pairs of as and bs
    assert (pairs.length == rets.length);
    results = new TreeMap();
    for (int i = 0; i < pairs.length; ++i) {
      // We can't just put because Java does not have multimaps
      LinkedList vals;
      if (results.containsKey(rets[i])) {
        vals = (LinkedList) results.get(rets[i]);
      } else {
        vals = new LinkedList();
        results.put(rets[i], vals);
      }
      vals.add(pairs[i]);
    }
  }

  public void merge(LevenshteinWorkerReturn other) {
    // We can't just putAll because Java does not have multimaps
    Iterator keyIt = other.results.keySet().iterator();
    Iterator valIt = other.results.values().iterator();
    while (keyIt.hasNext() && valIt.hasNext()) {
      int dist = (int) keyIt.next();
      LinkedList stringPairs = (LinkedList) valIt.next();

      LinkedList vals;
      if (results.containsKey(dist)) {
        vals = (LinkedList) results.get(dist);
      } else {
        vals = new LinkedList();
        results.put(dist, vals);
      }
      vals.addAll(stringPairs);
    }
  }

  public void resize() {resize(MAX_VALUES);}
  public void resize(int size) {
    // Only keep the top MAX_VALUES results
    int currentResult = 0;
    Iterator valIt = results.values().iterator();
    while (valIt.hasNext()) {
      LinkedList strPairs = (LinkedList) valIt.next();
      Iterator sIt = strPairs.iterator();
      while (sIt.hasNext()) {
        sIt.next();
        if (currentResult++ >= size)
          sIt.remove();
      }
    }
  }

  public void print() {
    // Parallel iterators because Java can't iterate over (key,val) pairs
    Iterator keyIt = results.keySet().iterator();
    Iterator valIt = results.values().iterator();
    while (keyIt.hasNext() && valIt.hasNext()) {
      int dist = (int) keyIt.next();
      LinkedList strPairs = (LinkedList) valIt.next();
      Iterator strIt = strPairs.iterator();
      while (strIt.hasNext()) {
        StringPair strings = (StringPair) strIt.next();
        System.out.printf("distance(%s, %s) = %d\n",
          strings.str1, strings.str2, dist);
      }
    }
  }
}
