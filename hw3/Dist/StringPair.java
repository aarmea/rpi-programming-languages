// Because Java does not have pairs
class StringPair implements java.io.Serializable {
  public String str1, str2;

  public StringPair(String a, String b) {
    str1 = a;
    str2 = b;
  }
}
