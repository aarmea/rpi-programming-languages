CSCI-4430 Assignment 3
======================

Submission by [Albert Armea](mailto:armeaa@rpi.edu), based on
[this problem](http://www.cs.rpi.edu/academics/courses/spring13/proglang/PA3.htm).

I decided to use [SALSA](http://wcl.cs.rpi.edu/salsa/) 1.1.5, the recommended
language, for this assignment.

I placed `salsa1.1.5.jar` in `~/Apps/salsa/salsa1.1.5.jar`. My BASH
`~/.profile` contains the following:

    # SALSA stuff, http://wcl.cs.rpi.edu/salsa/
    export SWPATH=~/Apps/
    export SALSAPATH=$SWPATH/Salsa
    export SALSAVER=1.1.5
    export SALSAOPTS 
    # SALSA 1.x aliases
    alias salsac='java -cp $SALSAPATH/salsa$SALSAVER.jar:. salsac.SalsaCompiler *.salsa; javac -classpath $SALSAPATH/salsa$SALSAVER.jar:. *.java'
    alias salsa='java -cp $SALSAPATH/salsa$SALSAVER.jar:. $SALSAOPTS'
    alias wwcns='java -cp $SALSAPATH/salsa$SALSAVER.jar:. wwc.naming.WWCNamingServer'
    alias wwctheater='java -cp $SALSAPATH/salsa$SALSAVER.jar:. $SALSAOPTS wwc.messaging.Theater'

Part 1: Concurrent Programming
------------------------------

Assuming SALSA is set up as outlined above, compile this part of the
assignment by running `salsac Dist.java` within the `Dist` directory. Then you
can run the assignment with `salsa Dist ../sequences.txt 10`.

Part 2: Distributed Programming
-------------------------------

Compile part 1 as above. Then you can run this part of the assignment with
`salsa Dist ../sequences.txt 10 ../theaters.txt`. `theaters.txt` expects the
locations of the WWC nameserver on the first line and theaters on the
remaining lines.

My `theaters.txt` contains the following:

    127.0.0.1:3030
    127.0.0.1:4040
    127.0.0.1:4041
    127.0.0.1:4042

To initalize this configuration, I ran `wwcns`, `wwctheater`, `wwctheater 4041`,
`wwctheater 4042` in separate terminal sessions.

On my machine, there appears to be a bug in which repeatedly running this part
of the assignment causes the nameserver to consume large amounts of CPU after
my assignment code completes and exits. I cannot determine whether this bug is
in my code or is a problem with the SALSA implementation.

Extra Credit: Load Balancing
----------------------------

This part of the assignment has not yet been implemented.
